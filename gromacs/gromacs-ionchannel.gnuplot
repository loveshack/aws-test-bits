set terminal pngcairo
set output "gromacs-ionchannel.png"
set xlabel "Nodes"
set ylabel "ns/day"
set xrange [0:7]
set key off
set monochrome
set title "Gromacs ion channel on c5n.9xlarge nodes (18 core)"
plot '-' notitle pt 4
1 16.4
2 14.6
3 30.0
4 33.9
4 19.8
4 33.7
4 19.7
4 33.8
5 40.9
6 31.6
e
