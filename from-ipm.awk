#!/usr/bin/awk -f
BEGIN {print "Nodes Cores Wallclock(s) %MPI %I/O LB CE PE"}
/%comm / {cores=$4; nodes=$6; mpi=$10}
/^# wallclock / {
    wallc=$7; if (wallc > 100) wallc=int(wallc)
    wallc_avg=$5; wallc_min=$6; wallc_max=$7 }
/%i\/o / {io=int($6)}
/^# MPI / {mpi_avg=$5; mpi_min=$6;
    ut_avg=wallc_avg - mpi_avg; ut_max=wallc_max-mpi_min
    lb=100.0*ut_avg/ut_max
    ce=100.0*ut_max/wallc
    pe=lb*ce/100.0
    printf ("%d %d %s %d %d %.1f %.1f %.1f\n", nodes, cores,
            wallc, mpi, io, lb, ce, pe) }
