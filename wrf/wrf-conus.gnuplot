set terminal pngcairo
set output "wrf-conus.png"
set xlabel "Nodes"
set ylabel "Runtime (s)"
set xrange [0:9]
set key off
set monochrome
set title "WRF CONUS 12km on c5n.9xlarge nodes (18 core)"
plot '-' notitle pt 4
1 217
1 274
1 283
2 123
2 130
2 134
2 144
2 150
2 151
2 177
3 103
3 103
3 103
3 128
3 86
4 113
4 82
4 84
4 85
4 85
4 86
5 60
5 61
5 61
5 61
5 61
5 61
5 62
5 62
6 59
6 59
6 61
6 61
6 88
6 88
7 75
7 49
7 49
7 75
7 49
8 74
8 46
8 46
8 45
8 46
e
