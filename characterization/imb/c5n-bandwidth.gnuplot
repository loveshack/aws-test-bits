set terminal pngcairo
set output "c5n-bandwidth.png"
set xlabel "Bandwidth (MB/s)"
set ylabel "Binned frequency"
set yrange [0:10]
set key off
set monochrome
set title "2-process IMB 2097152-byte pingpong bandwidth, c5n nodes"
binwidth = 100
bin(val) = binwidth * floor(val/binwidth)
plot '-' using (bin(column(1))):(1.0) smooth frequency pt 5 ps 2
336.95
340.94
1422.04
1481
1821.22
1862.64
1871.20
1872.32
1890.64
1897.82
1898.16
1898.61
1905.80
1929.99
1933.47
1934.91
1937.35
1963.04
1994.21
1997.46
2011.96
2027.01
2027.98
2050.19
2051.93
2054.41
2091.38
2095.72
2105.11
2110.14
2111.40
2117.42
2124.49
2128.90
2129.52
e
