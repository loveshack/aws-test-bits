set terminal pngcairo
set output "c5n-latency.png"
set xlabel "Latency (μs)"
set ylabel "Binned frequency"
set key off
set monochrome
set xrange[25:36]
set yrange[0:10]
set title "2-process IMB 8-byte pingpong latency, c5n nodes"
binwidth = 1
bin(val) = binwidth * floor(val/binwidth)
plot '-' using (bin(column(1))):(1.0) smooth frequency pt 5 ps 2
26.99
27.35
27.77
27.92
27.96
27.98
28.01
28.05
28.15
28.26
28.57
28.60
28.67
28.72
28.81
29.03
29.32
29.47
29.53
29.81
30.46
30.64
30.71
31.01
31.20
31.52
31.83
31.84
32.66
33.16
33.65
34.82
35.13
35.20
e
