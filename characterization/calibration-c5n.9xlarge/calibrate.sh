#!/bin/sh

#$ -V -pe mpi 36 -cwd -j y -N simgrid-cal
exec mpirun --report-bindings -n 2 --map-by node --bind-to core smpi-calibrate -f /usr/share/simgrid-calibration/template/testplatform.xml -s /usr/share/simgrid-calibration/zoo_sizes -d `pwd` -p c5n -n 100
