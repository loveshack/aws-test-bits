set terminal pngcairo
set output "cp2k-h2o128.png"
set xlabel "Nodes"
set ylabel "Runtime (s)"
set xrange [0:7]
set key off
set monochrome
set title "CP2K H_2O-128 on c5n.9xlarge nodes (18 core)"
plot '-' notitle pt 4
1     241    
1     240    
1     241    
1     240    
1     258    
1     239    
1     239    
2     140    
2     140    
2     142    
2     140    
2     141    
2     141    
2     167    
3     111    
3     111    
3     112    
3     112    
3     113    
3     112    
3     144    
4     116    
4     107    
4     120    
4     110    
4     125    
4     125    
4     137    
5     112    
5     113    
5     112    
5     110    
5     143    
5     145    
5     145    
5     143    
5     144    
5     108    
5     108    
6     95.41  
6     95.41  
6     95.15  
6     94.41  
6     94.70
e
