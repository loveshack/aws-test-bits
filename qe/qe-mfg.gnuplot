set terminal pngcairo
set output "qe-au112-mfg.png"
set title "QE AU112 scaling on 32(?)-core SKX+OPA"
set xlabel "Cores"
set ylabel "Relative perfomance"
set key off
set xtics 32
set yrange [0:10]
set xrange[0:356]
set monochrome
plot '-' with linespoints pt 4
32 1.9
64 2.7
96 4.0
128 6.7
160 5.7
192 6.4
256 8.3
320 8.8
e

