set terminal pngcairo
set output "dl_poly-bench4.png"
set xlabel "Nodes"
set ylabel "Runtime (s)"
set xrange [0:9]
set key off
set monochrome
set title "dl\\_poly classic bench4 on c5n.9xlarge nodes (18 core)"
plot '-' notitle pt 4
1 18.4
2 13.2
3 26.2
3 11.7
3 21.9
3 11.7
3 21.9
4 11.4
4 25.9
4 11.4
5 11.7
8 21.3  
1 18.41 
1 18.37 
1 18.33 
1 18.43 
1 18.38 
2 13.50 
2 13.47 
2 13.41 
2 13.44 
2 13.56 
3 11.37 
3 11.89 
3 11.49 
3 11.65 
3 11.51 
4 11.39 
4 11.32 
4 11.34 
4 11.10 
4 11.23 
5 11.19 
5 11.16 
5 10.98 
5 11.25 
5 11.20 
e
