set terminal pngcairo
set output "nwchem-c240.png"
set xlabel "Nodes"
set ylabel "Runtime (s)"
set xrange [0:13]
set key off
set monochrome
set title "NWCHEM C_{240} Buckyball on c5n.9xlarge nodes (18 core)"
plot '-' notitle pt 4
1 3756
2 3081
3 1995
3 2035
3 2040
3 2042
3 2057
3 2082
3 2143
3 3081
3 3100
3 3101
3 3519
3 4588
3 4639
3 4824
3 5042
4 1690
4 1769
4 2537
4 2549
4 2565
4 2636
4 2661
4 4280
4 4363
4 4373
4 4414
4 4444
4 4477
4 4538
4 4656
8 2150
8 2372
12 2937
e
